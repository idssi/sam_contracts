var Ldm = artifacts.require("Ldm");

module.exports = function(done) {
  console.log("Getting deployed version of Ldm...")
  Ldm.deployed().then(function(instance) {
    return instance.signMisuraAa(0, {privateFor: ["BULeR8JyUWhiuuCMU/HLA0Q5pzkYT+cHII3ZKBey3Bo=",
                                                   "QfeDAys9MPDs2XHExtc84jKGHxZg/aj52DTh0vtA3Xc=",
                                                   "1iTZde/ndBHvzhcl7V68x44Vx7pl8nwx9LqnM/AfJUg=",
                                                   "oNspPPgszVUFw0qmGFfWwh1uxVUXgvBxleXORHj07g8="]});
  }).then(function(result) {
    console.log("Result:", result);
    console.log("Finished!");
    done();
  }).catch(function(e) {
    console.log(e);
    done();
  });
};