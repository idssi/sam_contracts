var Ldm = artifacts.require("Ldm");

module.exports = function(done) {
  console.log("Getting deployed version of Ldm...")
  Ldm.deployed().then(function(instance) {
    return instance.getLdmLength();
  }).then(function(result) {
    console.log("Result:", result);
    console.log("Transaction:", result.tx);
    console.log("Finished!");
    done();
  }).catch(function(e) {
    console.log(e);
    done();
  });
};