var Ldm = artifacts.require("Ldm");

module.exports = async function(done) {
  console.log("Getting deployed version of Ldm...")
  Ldm.deployed().then(function(instance) {
    return instance.setMisura("4235254","15/10/2019", "Qualcosa di grande", "3.000", "3,9%", "50",
                            {privateFor: ["BULeR8JyUWhiuuCMU/HLA0Q5pzkYT+cHII3ZKBey3Bo=",
                                          "QfeDAys9MPDs2XHExtc84jKGHxZg/aj52DTh0vtA3Xc=",
                                          "1iTZde/ndBHvzhcl7V68x44Vx7pl8nwx9LqnM/AfJUg=",
                                          "oNspPPgszVUFw0qmGFfWwh1uxVUXgvBxleXORHj07g8="]});
  }).then(function(result) {
    console.log("Result:", result.logs[0]);
    console.log("Transaction:", result.tx);
    console.log("Finished!");
    done();
  }).catch(function(e) {
    console.log(e);
    done();
  });
};