var Ldm = artifacts.require("Ldm");

module.exports = function(done) {
  console.log("Getting deployed version of Ldm...")
  Ldm.deployed().then(function(instance) {
    return instance.getAllLdm();
  }).then(function(result) {
    console.log("Result:", result);
    console.log("Finished!");
    done();
  }).catch(function(e) {
    console.log(e);
    done();
  });
};