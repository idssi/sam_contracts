var Gdl = artifacts.require("Gdl");

module.exports = function(done) {
  console.log("Getting deployed version of Gdl...")
  Gdl.deployed().then(function(instance) {
    return instance.getLavori();
  }).then(function(result) {
    console.log("Result:", result);
    console.log("Finished!");
    done();
  }).catch(function(e) {
    console.log(e);
    done();
  });
};