pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract Gdl {

// Struttura del giornale dei lavori
struct struct_Giornale {
    string data;
    string meteo;
    string annotazioni;
    string operai;
    string mezzi;
    string path_allegato;
    bool approvato;
  }

struct_Giornale[] public lavoro;
// Evento che segnala l'avvenuto inserimento nel giornale
  event hackSetLavoro(bool, struct_Giornale);

  // Evento che segnala l'avvenuta firma della voce nel Giornale
  event hackSign(bool, string, uint);

// Inserisci nuovo lavoro
function setLavoro(string memory _data, string memory _meteo, string memory _annotazioni,
string memory _operai, string memory _mezzi, string memory _path_allegato) public  {
    struct_Giornale memory newData = struct_Giornale({
            data : _data,
            meteo : _meteo,
            annotazioni : _annotazioni,
            operai: _operai,
            mezzi: _mezzi,
            path_allegato: _path_allegato,
            approvato: false
        });
        lavoro.push(newData);
        // Segnaliamo l'aggiunta della misura
        emit hackSetLavoro(true, newData);
    }

// Ottengo informazioni di un dato lavoro
function getLavoroInfo(uint ID) public view returns (string memory, string memory, string memory,
  string memory,string memory,string memory, bool){
    struct_Giornale memory o = lavoro[ID];
    return (o.data, o.meteo, o.annotazioni, o.operai, o.mezzi, o.path_allegato, o.approvato);
  }

// funzione di approvazione nuovo lavoro -- solamente il Direttore dei lavori può approvarlo
function approvaLavoro(uint ID) public {
  lavoro[ID].approvato = true;
  // Evento che segnala l'avvenuta firma della voce nel libretto delle misure
  emit hackSign(true, "Voce nel giornale approvata con successo", ID);
}

// Ottengo tutti i lavori
function getLavori() public view returns (struct_Giornale[] memory){
        return lavoro;
    }

// Ottengo il numero di tutti i lavori inseriti
function countGiornale() public view returns (uint){
        return lavoro.length;
    }

}
