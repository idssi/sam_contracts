pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract SalInit {

string oggettoInit = "Costruzione di un complesso di fabbricati destinati ad abitazione ed a negozi nel Comune di Milano";
string committenteInit = "Sviluppo Milanese s.r.l.";
string impresaInit = "La Costruttoria s.r.l";

string totalProgetto = "";

//Inserisce oggetto iniziale sal
function setOggettoInit(string memory _oggettoInit) public {
  oggettoInit = _oggettoInit;
}

//Inserisce committente iniziale sal
function setCommittenteInit(string memory _committenteInit) public {
  committenteInit = _committenteInit;
}

//Inserisce impresa iniziale sal
function setImpresaInit(string memory _impresaInit) public {
  impresaInit = _impresaInit;
}

//Inserisce il total price iniziale sal
function settotalProgettoInit(string memory _totalProgettoInit) public {
  impresaInit = _totalProgettoInit;
}

//Restituisce oggetto iniziale sal
function getOggettoInit() public view returns (string memory){
        return oggettoInit;
}

//Restituisce committente iniziale sal
function getCommittenteInit() public view returns (string memory){
        return committenteInit;
}

//Restituisce impresa iniziale sal
function getImpresaInit() public view returns (string memory){
        return impresaInit;
}

function gettotalProgettoInit() public view returns (string memory){
        return totalProgetto;
}

}