pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract Ldm {
// Struttura per ogni voce nel libretto delle misure
struct Misura {
    string tar;
    string data;
    string des_lav;
    string costo;
    string aliq;
    string tot;
    bool sign_ddl;
    bool sign_aa;
  }
  // Struttura Ausiliaria contro lo Stack to deep
  struct Signed {
    bool ddl;
    bool aa;
  }

  // Libretto delle misure
  Misura[] ldm;
  // Evento che segnala l'avvenuto inserimento ne libretto delle misure
  event hackSetMisura(bool, Misura);

  // Evento che segnala l'avvenuta firma della voce nel libretto delle misure
  event hackSignDdl(bool, string, uint);
  event hackSignAa(bool, string, uint);

  // Funzione per aggiungere una voce al libretto delle misure
  function setMisura(string memory _tar, string memory _data, string memory _des_lav,
    string memory _costo, string memory _aliq, string memory _tot) public {
      Misura memory misura = Misura({
      tar: _tar,
      data: _data,
      des_lav: _des_lav,
      costo: _costo,
      aliq: _aliq,
      tot: _tot,
      sign_ddl: false,
      sign_aa: false
    });
    ldm.push(misura);
    // Segnaliamo l'aggiunta della misura
    emit hackSetMisura(true, misura);
  }

  // Funzione per ottenere tutto il libretto delle misure
  function getAllLdm() public view returns(Misura[] memory misura) {
    return(ldm);
  }

  // Funzione per ottenere una particolare voce del libretto delle misure
  function getMisura(uint index) public view returns(string memory tar, string memory data, string memory des_lav,
  string memory costo, string memory aliq, string memory tot, Signed memory sign) {
    Signed memory signed;
    signed.ddl = ldm[index].sign_ddl;
    signed.aa = ldm[index].sign_aa;
    return (
      ldm[index].tar,
      ldm[index].data,
      ldm[index].des_lav,
      ldm[index].costo,
      ldm[index].aliq,
      ldm[index].tot,
      signed
      );
}
  // Funzione per ottenere la lunghezza del libretto delle misure
  function getLdmLength() public view returns(uint ldm_length) {
    return(ldm.length);
  }

  // Funzione per firmare una particolare voce del libretto al Direttore dei lavori
  function signMisuraDdl(uint index) public {
      ldm[index].sign_ddl = true;
      // Segnaliamo la firma del direttore dei lavori
      emit hackSignDdl(true, 'Firma del direttore deavorii l avvenuta con successo', index);
  }

  //Funzione per firmare una particolare voce del libretto all appaltatore
  function signMisuraAa(uint index) public {
      ldm[index].sign_aa = true;
      // Segnaliamo la firma dell'appaltatore
      emit hackSignAa(true, "Firma dell'appaltatore avvenuta con successo", index);
  }
}