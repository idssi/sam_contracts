pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract RcInit {

string oggettoInit = "Costruzione di un complesso di fabbricati destinati ad abitazione ed a negozi nel Comune di Milano";
string committenteInit = "Sviluppo Milanese s.r.l.";
string impresaInit = "La Costruttoria s.r.l";
bool signRupInit = false;

event hackSignRup(bool);

//Inserisce oggetto iniziale rc
function setOggettoInit(string memory _oggettoInit) public {
  oggettoInit = _oggettoInit;
}

//Inserisce committente iniziale rc
function setCommittenteInit(string memory _committenteInit) public {
  committenteInit = _committenteInit;
}

//Inserisce impresa iniziale rc
function setImpresaInit(string memory _impresaInit) public {
  impresaInit = _impresaInit;
}

//Inserisce firma rup iniziale rc
function setsignRupInit() public {
  signRupInit = true;
  emit hackSignRup(true);
}

//Restituisce oggetto iniziale rc
function getOggettoInit() public view returns (string memory){
        return oggettoInit;
}

//Restituisce committente iniziale rc
function getCommittenteInit() public view returns (string memory){
        return committenteInit;
}

//Restituisce impresa iniziale rc
function getImpresaInit() public view returns (string memory){
        return impresaInit;
}

//Restituisce firma rup iniziale rc
function getsignRupInit() public view returns (bool){
        return signRupInit;
}


}