pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract GdlInit {

string oggettoInit = "Costruzione di un complesso di fabbricati destinati ad abitazione ed a negozi nel Comune di Milano";
string committenteInit = "Sviluppo Milanese s.r.l.";
string impresaInit = "La Costruttoria s.r.l";

bool signImpresaInit = false;
bool signDirettoreInit = false;
bool signRupInit = false;

event hackSignImpresa(bool);
event hackSignDirettore(bool);
event hackSignRup(bool);

//Inserisce oggetto iniziale gdl
function setOggettoInit(string memory _oggettoInit) public {
  oggettoInit = _oggettoInit;
}

//Inserisce committente iniziale gdl
function setCommittenteInit(string memory _committenteInit) public {
  committenteInit = _committenteInit;
}

//Inserisce impresa iniziale gdl
function setImpresaInit(string memory _impresaInit) public {
  impresaInit = _impresaInit;
}

//Inserisce firma impresa iniziale gdl
function setsignImpresaInit() public {
  signImpresaInit = true;
  emit hackSignImpresa(true);
}

//Inserisce firma direttore iniziale gdl
function setsignDirettoreInit() public {
  signDirettoreInit = true;
  emit hackSignDirettore(true);
}

//Inserisce firma rup iniziale gdl
function setsignRupInit() public {
  signRupInit = true;
  emit hackSignRup(true);
}

//Restituisce oggetto iniziale gdl
function getOggettoInit() public view returns (string memory){
        return oggettoInit;
}

//Restituisce committente iniziale gdl
function getCommittenteInit() public view returns (string memory){
        return committenteInit;
}

//Restituisce impresa iniziale gdl
function getImpresaInit() public view returns (string memory){
        return impresaInit;
}

//Restituisce firma impresa iniziale gdl
function getsignImpresaInit() public view returns (bool){
        return signImpresaInit;
}

//Restituisce firma direttore iniziale gdl
function getsignDirettoreInit() public view returns (bool){
        return signDirettoreInit;
}

//Restituisce firma rup iniziale gdl
function getsignRupInit() public view returns (bool){
        return signRupInit;
}


}