pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract Email {

// Struttura del giornale dei lavori
struct struct_Email {
    string data;
    string mittente;
    string[] destinatario;
    string oggetto;
    string testo;
  }

struct_Email[] public email;

 event hackSetEmail(bool, struct_Email);
// Inserisci nuova email
function setEmail(string memory _data, string memory _mittente, string[] memory _destinatario, string memory _oggetto,
string memory _testo) public  {


    struct_Email memory newData = struct_Email({
            data : _data,
            mittente : _mittente,
            destinatario : _destinatario,
            oggetto : _oggetto,
            testo: _testo
        });
        email.push(newData);

        emit hackSetEmail(true, newData);
    }

// Ottengo tutte le email
function getEmail() public view returns (struct_Email[] memory){
        return email;
    }

function countEmail() public view returns (uint){
        return email.length;
    }

}
