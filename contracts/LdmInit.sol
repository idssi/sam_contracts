pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract LdmInit {

string oggettoInit = "Costruzione di un complesso di fabbricati destinati ad abitazione ed a negozi nel Comune di Milano";
string committenteInit = "Sviluppo Milanese s.r.l.";
string impresaInit = "La Costruttoria s.r.l";
bool signRupInit = false;

event hackSignRup(bool);

//Inserisce oggetto iniziale ldm
function setOggettoInit(string memory _oggettoInit) public {
  oggettoInit = _oggettoInit;
}

//Inserisce committente iniziale ldm
function setCommittenteInit(string memory _committenteInit) public {
  committenteInit = _committenteInit;
}

//Inserisce impresa iniziale ldm
function setImpresaInit(string memory _impresaInit) public {
  impresaInit = _impresaInit;
}

//Inserisce firma rup iniziale ldm
function setsignRupInit() public {
  signRupInit = true;
  emit hackSignRup(true);
}

//Restituisce oggetto iniziale ldm
function getOggettoInit() public view returns (string memory){
        return oggettoInit;
}

//Restituisce committente iniziale ldm
function getCommittenteInit() public view returns (string memory){
        return committenteInit;
}

//Restituisce impresa iniziale ldm
function getImpresaInit() public view returns (string memory){
        return impresaInit;
}

//Restituisce firma rup iniziale ldm
function getsignRupInit() public view returns (bool){
        return signRupInit;
}


}