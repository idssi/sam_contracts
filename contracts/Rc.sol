pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract Rc {

// Struttura del registro
struct struct_Registro {
    string tariffa;
    string data;
    string indicazione;
    string percentuale;
    string prezzoUnitario;
    string aliq;
    string debito;
    bool sign_AA;
    bool sign_DDL;
  }

  struct_Registro[] public registro;
  // Evento che segnala l'avvenuto inserimento ne libretto delle misure
  event hackSetRegistro(bool, struct_Registro);

  // Evento che segnala l'avvenuta firma della voce nel libretto delle misure
  event hackSignDdl(bool, string, uint);
  event hackSignAa(bool, string, uint);

  // Inserisci nuovo registro
  function setRegistro(string memory _tariffa, string memory _data, string memory _indicazione,
                      string memory _percentuale, string memory _prezzoUnitario,
                      string memory _aliq, string memory _debito, string memory _usr) public {

  struct_Registro memory newData = struct_Registro({
      tariffa : _tariffa,
      data : _data,
      indicazione: _indicazione,
      percentuale: _percentuale,
      prezzoUnitario: _prezzoUnitario,
      aliq: _aliq,
      debito: _debito,
      sign_AA: false,
      sign_DDL: false
    });
  registro.push(newData);
    // Segnaliamo l'aggiunta nel registro
    emit hackSetRegistro(true, newData);
  }

  // Ottengo informazioni di un dato registro
  function getRegistroinfo(uint ID) public view returns (string memory tariffa, string memory data, string memory indicazione,
                                                        string memory percentuale, string memory prezzoUnitario,
                                                        string memory aliq, string memory debito, bool sign_AA, bool sign_DDL) {
    struct_Registro memory rc = registro[ID];
    return (rc.tariffa, rc.data, rc.indicazione, rc.percentuale, rc.prezzoUnitario,
    rc.aliq, rc.debito, rc.sign_AA, rc.sign_DDL);
  }

  // Funzione di approvazione nuovo registro dall'impresa
  function approvaRegistroAA(uint ID) public {
    registro[ID].sign_AA = true;
    // Evento che segnala l'avvenuta firma della voce nel Registro
    emit hackSignAa(true, "Voce nel registro approvata con successo dal aa", ID);
  }

  // Funzione di approvazione nuovo registro dal direttore
  function approvaRegistroDDL(uint ID) public {
    registro[ID].sign_DDL = true;
    // Evento che segnala l'avvenuta firma della voce nel Registro
    emit hackSignDdl(true, "Voce nel registro approvata con successo dal ddl", ID);
  }

  // Ottengo informazioni di tutti i registri
  function getRegistri() public view returns (struct_Registro[] memory) {
    return registro;
  }
  // Ottengo il numero di tutti i registri
  function countRegistri() public view returns (uint) {
    return registro.length;
  }
}