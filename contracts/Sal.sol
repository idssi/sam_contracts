pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract Sal {
// Struttura dello Stato Avanzamento Lavori
struct struct_Sal {
    string tariffa;
    string data;
    string indicazione;
    string quantita;
    string importoUnitario;
    string aliq;
    string tot;
  }

struct_Sal[] public sal;
// Evento che segnala l'avvenuto inserimento ne libretto delle misure
event hackSetSal(bool, struct_Sal);

//Account
  string ddl = '0xed9d02e382b34818e88B88a309c7fe71E65f419d';
  string aa =  '0x0fBDc686b912d7722dc86510934589E0AAf3b55A';

//Funzione inserimento nuovo Sal
function setSal(string memory _tariffa, string memory _data,
                string memory _indicazione, string memory _quantita,
                string memory _importoUnitario, string memory _aliq, string memory _tot, string memory _usr) public {
 struct_Sal memory newData = struct_Sal({
    tariffa : _tariffa,
    data : _data,
    indicazione: _indicazione,
    quantita: _quantita,
    importoUnitario: _importoUnitario,
    aliq: _aliq,
    tot: _tot
   });
 sal.push(newData);
 // Segnaliamo l'aggiunta nel sal
 emit hackSetSal(true, newData);
}

// Ottengo un dato Sal
function getSalinfo(uint ID) public view returns (string memory tar, string memory data, string memory indicazione,
                                                  string memory quantita, string memory importoUnitario, string memory aliq, string memory tot)
{
  struct_Sal memory sl = sal[ID];
  return (sl.tariffa, sl.data, sl.indicazione,
  sl.quantita, sl.importoUnitario, sl.aliq, sl.tot);
}
// Ottengo tutti i Sal
function getSal() public view returns (struct_Sal[] memory) {
  return sal;
}

//Ottengo il numero di Sal inseriti
function countSal() public view returns (uint) {
  return sal.length;
}


}